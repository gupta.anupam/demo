package com.example.demo.persistance;

import java.util.List;

import com.example.demo.model.User;
import com.example.demo.util.SearchCriteria;
import org.springframework.stereotype.Repository;

public interface IUserDAO {
    List<User> searchUser(List<SearchCriteria> params);

    void save(User entity);
}
