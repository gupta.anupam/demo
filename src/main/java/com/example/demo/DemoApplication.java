package com.example.demo;

import com.example.demo.model.User;
import com.example.demo.persistance.IUserDAO;
import com.example.demo.util.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@ComponentScan({"com.example.demo"})
@EnableJpaRepositories("com.example.demo.persistance")
@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	@Autowired
	private IUserDAO userApi;


	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	@Override
	@Transactional
	public void run(String... args) throws Exception {

		User userJohn = new User();
		userJohn.setFirstName("john");
		userJohn.setLastName("doe");
		userJohn.setEmail("john@doe.com");
		userJohn.setAge(22);
		userApi.save(userJohn);

		User userTom = new User();
		userTom.setFirstName("tom");
		userTom.setLastName("doe");
		userTom.setEmail("tom@doe.com");
		userTom.setAge(26);
		userApi.save(userTom);

		final List<SearchCriteria> params = new ArrayList<SearchCriteria>();
		params.add(new SearchCriteria("firstName", ":", "john"));
		params.add(new SearchCriteria("lastName", ":", "doe"));

		final List<User> results = userApi.searchUser(params);
		System.out.println(results);
	}

}
